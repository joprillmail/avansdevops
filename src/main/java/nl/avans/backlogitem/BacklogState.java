package nl.avans.backlogitem;

import nl.avans.user.User;

public interface BacklogState {
    void todo();
    void doing(User user);      // Toekennen developer
    void readyForTesting();
    void testing(User user);    // Toekennen tester
    void tested(User user);     // Toekennen lead-developer
    void done();
}
