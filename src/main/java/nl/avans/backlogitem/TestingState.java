package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestingState implements BacklogState {
    private final BacklogItem backlogItem;

    public TestingState(BacklogItem backlogItem){
        this.backlogItem = backlogItem;
    }

    @Override
    public void todo() {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back in Todo");
    }

    @Override
    public void doing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back in Doing");
    }

    @Override
    public void readyForTesting() {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back in Ready for Testing");

    }

    @Override
    public void testing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is already Testing");
    }

    @Override
    public void tested(User user) {
        backlogItem.setUser(user);
        backlogItem.notifyObservers();
        backlogItem.setState(new TestedState(backlogItem));
    }

    @Override
    public void done() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item has not been tested yet");
    }
}
