package nl.avans.backlogitem;

import nl.avans.notification.Subject;
import nl.avans.notification.Observer;
import nl.avans.user.User;

import java.util.ArrayList;
import java.util.List;

public class BacklogItem implements Subject {
    private final List<Observer> observers;

    private final int ID;
    private final String taskName;
    private final String taskDescription;
    private final int priority;
    private User user;

    private final BacklogState todoState;
    private final BacklogState doingState;
    private final BacklogState readyForTestingState;
    private final BacklogState testingState;
    private final BacklogState testedState;
    private final BacklogState doneState;

    private BacklogState backlogState;

    public BacklogItem(int ID, String taskName, String taskDescription, int priority){
        this.observers = new ArrayList<>();

        this.ID = ID;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.priority = priority;

        this.todoState = new TodoState(this);
        this.doingState = new DoingState(this);
        this.readyForTestingState = new ReadyForTestingState(this);
        this.testingState = new TestingState(this);
        this.testedState = new TestedState(this);
        this.doneState = new DoneState(this);

        this.backlogState = todoState;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTaskName(){
        return this.taskName;
    }

    public User getUser() {
        return user;
    }

    public BacklogState getTodoState() {
        return todoState;
    }

    public BacklogState getDoingState() {
        return doingState;
    }

    public BacklogState getReadyForTestingState() {
        return readyForTestingState;
    }

    public BacklogState getTestingState() {
        return testingState;
    }

    public BacklogState getTestedState() {
        return testedState;
    }

    public BacklogState getDoneState() {
        return doneState;
    }

    public void setState(BacklogState state){
        this.backlogState = state;
    }

    public String getState(){
        return this.backlogState.getClass().getSimpleName();
    }

    public List<Observer> getObservers() {
        return observers;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(Observer observer : observers){
            observer.update();
        }
    }
}
