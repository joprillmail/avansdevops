package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadyForTestingState implements BacklogState {
    private final BacklogItem backlogItem;

    public ReadyForTestingState(BacklogItem backlogItem){
        this.backlogItem = backlogItem;
    }

    @Override
    public void todo() {
        backlogItem.setUser(null);
        backlogItem.setState(new TodoState(backlogItem));
    }

    @Override
    public void doing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item cannot be put back into Doing");
    }

    @Override
    public void readyForTesting() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is already in Ready for Testing");
    }

    @Override
    public void testing(User user) {
        backlogItem.setUser(user);
        backlogItem.setState(new TestingState(backlogItem));
    }

    @Override
    public void tested(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is not testing now");
    }

    @Override
    public void done() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item has not been tested yet");
    }
}
