package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DoingState implements BacklogState {
    private final BacklogItem backlogItem;

    public DoingState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void todo() {
        backlogItem.notifyObservers();

        backlogItem.setUser(null);
        backlogItem.setState(new TodoState(backlogItem));
    }

    @Override
    public void doing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is already in Doing");
    }

    @Override
    public void readyForTesting() {
        backlogItem.notifyObservers();
        backlogItem.setState(new ReadyForTestingState(backlogItem));
    }

    @Override
    public void testing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is not ready for testing yet");
    }

    @Override
    public void tested(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is not testing now");
    }

    @Override
    public void done() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item has not been tested yet");
    }
}
