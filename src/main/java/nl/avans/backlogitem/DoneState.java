package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DoneState implements BacklogState {
    private final BacklogItem backlogItem;

    public DoneState(BacklogItem backlogItem){
        this.backlogItem = backlogItem;
    }

    private void logger(String method) {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back in " + method);
    }

    @Override
    public void todo() {
        logger("ToDo");
    }

    @Override
    public void doing(User user) {
        logger("Doing");
    }

    @Override
    public void readyForTesting() {
        logger("ReadyForTesting");
    }

    @Override
    public void testing(User user) {
        logger("Testing");
    }

    @Override
    public void tested(User user) {
        logger("Tested");
    }

    @Override
    public void done() {
        logger("Done");
    }
}
