package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestedState implements BacklogState {
    private final BacklogItem backlogItem;

    public TestedState(BacklogItem backlogItem){
        this.backlogItem = backlogItem;
    }

    @Override
    public void todo() {
        backlogItem.setUser(null);
        backlogItem.setState(new TodoState(backlogItem));
    }

    @Override
    public void doing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back into Doing");
    }

    @Override
    public void readyForTesting() {
        backlogItem.notifyObservers();
        backlogItem.setState(new ReadyForTestingState(backlogItem));
    }

    @Override
    public void testing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Cannot put item back in Testing");
    }

    @Override
    public void tested(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is already in Tested");
    }

    @Override
    public void done() {
        backlogItem.setState(new DoneState(backlogItem));
    }
}
