package nl.avans.backlogitem;

import nl.avans.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TodoState implements BacklogState {
    private final BacklogItem backlogItem;

    public TodoState(BacklogItem backlogItem){
        this.backlogItem = backlogItem;
    }

    @Override
    public void todo() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is already in ToDo");
    }

    @Override
    public void doing(User user) {
        this.backlogItem.setUser(user);
        this.backlogItem.setState(new DoingState(backlogItem));
    }

    @Override
    public void readyForTesting() {
        Logger.getAnonymousLogger().log(Level.INFO, "Item hasn't been made yet");
    }

    @Override
    public void testing(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is not ready for testing yet");
    }

    @Override
    public void tested(User user) {
        Logger.getAnonymousLogger().log(Level.INFO, "Item is not testing now");
    }

    @Override
    public void done() {
        this.backlogItem.setState(new DoneState(backlogItem));
    }
}
