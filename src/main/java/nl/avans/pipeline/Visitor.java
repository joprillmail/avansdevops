package nl.avans.pipeline;

import java.awt.*;

public abstract class Visitor {
    public abstract void visitPipeline(Pipeline pipeline);
    public abstract void visitImage(Image image);
    public abstract void visitJob(Job job);
    public abstract void visitCommand(Command command);
}
