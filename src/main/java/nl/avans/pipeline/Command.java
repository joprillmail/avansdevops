package nl.avans.pipeline;

public class Command extends CompositeComponent{
    private final String cmd;

    public Command(String cmd) {
        this.cmd = cmd;
    }

    public String getCommand(){
        return cmd;
    }

    public void acceptVisitor(Visitor visitor){
        visitor.visitCommand(this);
        super.acceptVisitor(visitor);
    }
}
