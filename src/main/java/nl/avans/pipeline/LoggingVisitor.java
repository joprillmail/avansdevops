package nl.avans.pipeline;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingVisitor extends Visitor{
    public LoggingVisitor() {
    }

    @Override
    public void visitPipeline(Pipeline pipeline){
        Logger.getAnonymousLogger().log(Level.INFO, "Start pipeline " + pipeline.getPipelineName());
    }

    @Override
    public void visitImage(Image image) {
        Logger.getAnonymousLogger().log(Level.INFO, "Load image " + image.getImageName());
    }

    @Override
    public void visitJob(Job job) {
        Logger.getAnonymousLogger().log(Level.INFO, "Run job " + job.getJobName());
    }

    @Override
    public void visitCommand(Command command) {
        Logger.getAnonymousLogger().log(Level.INFO, "execute " + command.getCommand());
    }
}
