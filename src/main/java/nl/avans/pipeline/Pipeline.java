package nl.avans.pipeline;

public class Pipeline extends CompositeComponent {
    private String pipelineName;

    public Pipeline(String pipelineName){
        this.pipelineName = pipelineName;
    }

    public String getPipelineName(){
        return pipelineName;
    }

    public void acceptVisitor(Visitor visitor){
        visitor.visitPipeline(this);
        super.acceptVisitor(visitor);
    }
}
