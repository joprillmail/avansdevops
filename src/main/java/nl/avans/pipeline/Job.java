package nl.avans.pipeline;

public class Job extends CompositeComponent{
    private final String jobName;

    public Job(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName(){
        return jobName;
    }

    public void acceptVisitor(Visitor visitor){
        visitor.visitJob(this);
        super.acceptVisitor(visitor);
    }
}
