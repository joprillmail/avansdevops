package nl.avans.pipeline;

public class Image extends CompositeComponent {
    private final String imageName;

    public Image(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName(){
        return imageName;
    }

    public void acceptVisitor(Visitor visitor){
        visitor.visitImage(this);
        super.acceptVisitor(visitor);
    }
}
