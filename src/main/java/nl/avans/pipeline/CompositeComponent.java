package nl.avans.pipeline;

import java.util.ArrayList;

public class CompositeComponent extends Component {
    private final ArrayList<Component> parts;

    public CompositeComponent() {
        this.parts = new ArrayList<>();
    }

    public void addComponent(Component comp){
        this.parts.add(comp);
    }

    public Component getComponent(int i){
        return parts.get(i);
    }

    public int getSize(){
        return this.parts.size();
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        for (Component component : parts){
            component.acceptVisitor(visitor);
        }
    }
}
