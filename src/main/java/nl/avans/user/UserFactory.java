package nl.avans.user;

public class UserFactory {
    public User registerUser(UserType userType, String username, String email, ProjectRole projectRole, String password){
        if (userType == UserType.STUDENT){
            return new Student(username, email, projectRole, password);
        }
        else if (userType == UserType.TEACHER){
            return new Teacher(username, email, projectRole, password);
        }
        else {
            return new Other(username, email, projectRole, password);
        }
    }
}
