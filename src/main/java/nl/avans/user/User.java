package nl.avans.user;

public abstract class User {
    private final String username;
    private final String emailAddress;
    private final ProjectRole projectRole;
    private final String password;

    User(String username, String emailAddress, ProjectRole projectRole, String password) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.projectRole = projectRole;
        this.password = password;
    }
}
