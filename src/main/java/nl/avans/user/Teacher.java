package nl.avans.user;

import java.security.SecureRandom;

public class Teacher extends User{
    private final int teacherId;

    public Teacher(String username, String emailAddress, ProjectRole projectRole, String password) {
        super(username, emailAddress, projectRole, password);
        this.teacherId = (int) Math.floor(new SecureRandom().nextDouble() * 9999);
    }
}
