package nl.avans.user;

public enum ProjectRole {
    SCRUM_MASTER,
    PRODUCT_OWNER,
    DEVELOPER,
    TESTER,
    LEAD_DEVELOPER
}
