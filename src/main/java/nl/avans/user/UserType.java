package nl.avans.user;

public enum UserType {
    STUDENT,
    TEACHER,
    OTHER
}
