package nl.avans.user;

import java.security.SecureRandom;
import java.util.Random;

public class Student extends User{
    private final int studentId;

    public Student(String username, String emailAddress, ProjectRole role, String password) {
        super(username, emailAddress, role, password);
        this.studentId = (int) Math.floor(new SecureRandom().nextDouble() * 9999);
    }
}
