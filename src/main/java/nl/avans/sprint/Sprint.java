package nl.avans.sprint;

import nl.avans.backlogitem.BacklogItem;
import nl.avans.user.User;

import java.util.ArrayList;

public class Sprint {
    private final int ID;
    private final String sprintName;
    private final String sprintDescription;
    private final SprintType sprintType;
    private final ArrayList<User> users;
    private final ArrayList<BacklogItem> backlogItems;

    private final SprintState createState;
    private final SprintState executeState;
    private final SprintState deployState;
    private final SprintState reviewState;
    private final SprintState doneState;

    private SprintState sprintState;

    public Sprint(int ID, String sprintName, String sprintDescription, SprintType sprintType) {
        this.ID = ID;
        this.sprintName = sprintName;
        this.sprintDescription = sprintDescription;
        this.sprintType = sprintType;
        this.users = new ArrayList<>();
        this.backlogItems = new ArrayList<>();

        this.createState = new CreateState(this);
        this.executeState = new ExecuteState(this);
        this.deployState = new DeployState(this);
        this.reviewState = new ReviewState(this);
        this.doneState = new DoneState(this);

        this.sprintState = createState;
    }

    public void addUser(User user){
        this.users.add(user);
    }

    public void removeUser(User user){
        this.users.remove(user);
    }

    public void addBacklogItem(BacklogItem backlogItem){
        this.backlogItems.add(backlogItem);
    }

    public void removeBacklogItem(BacklogItem backlogItem){
        this.backlogItems.remove(backlogItem);
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public ArrayList<BacklogItem> getBacklogItems() {
        return backlogItems;
    }

    public SprintType getSprintType() {
        return sprintType;
    }

    public SprintState getCreateState() {
        return createState;
    }

    public SprintState getExecuteState() {
        return executeState;
    }

    public SprintState getDeployState() {
        return deployState;
    }

    public SprintState getReviewState() {
        return reviewState;
    }

    public SprintState getDoneState() {
        return doneState;
    }

    public void setState(SprintState state){
        this.sprintState = state;
    }

    public String getState(){
        return this.sprintState.getClass().getSimpleName();
    }
}
