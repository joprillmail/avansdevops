package nl.avans.sprint;

import nl.avans.pipeline.Pipeline;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DeployState implements SprintState{
    private final Sprint sprint;

    public DeployState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void create() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has already been created");
    }

    @Override
    public void execute() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has already been executed");
    }

    @Override
    public void deploy() {
        // TODO Implement pipeline execute
        Logger.getAnonymousLogger().log(Level.INFO, "Retry pipeline...");

        Logger.getAnonymousLogger().log(Level.INFO, "State will not been changed");
    }

    @Override
    public void review() {
        Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a review state");
    }

    @Override
    public void done() {
        this.sprint.setState(new DoneState(this.sprint));
    }
}
