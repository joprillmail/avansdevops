package nl.avans.sprint;

public enum SprintType {
    DEVELOPMENT,
    DEPLOYMENT
}
