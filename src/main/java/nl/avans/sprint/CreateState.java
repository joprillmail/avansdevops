package nl.avans.sprint;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateState implements SprintState {
    private final Sprint sprint;

    public CreateState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void create() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint is already in this state");
    }

    @Override
    public void execute() {
        sprint.setState(new ExecuteState(this.sprint));
    }

    @Override
    public void deploy() {
        if (this.sprint.getSprintType() == SprintType.DEPLOYMENT) Logger.getAnonymousLogger().log(Level.INFO, "Sprint has not been executed yet");
        else Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a deployment state");
    }

    @Override
    public void review() {
        if (this.sprint.getSprintType() == SprintType.DEVELOPMENT) Logger.getAnonymousLogger().log(Level.INFO, "Sprint has not been executed yet");
        else Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a review state");
    }

    @Override
    public void done() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has not been reviewed / deployed yet");
    }
}
