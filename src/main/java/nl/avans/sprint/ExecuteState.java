package nl.avans.sprint;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExecuteState implements SprintState{
    private final Sprint sprint;

    public ExecuteState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void create() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has already been created");
    }

    @Override
    public void execute() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint is already executing");
    }

    @Override
    public void deploy() {
        if (this.sprint.getSprintType() == SprintType.DEPLOYMENT) {
            // TODO Start executing pipeline
            Logger.getAnonymousLogger().log(Level.INFO, "Starting Pipeline...");

            this.sprint.setState(new DeployState(this.sprint));
        }
        else Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a deploy state");
    }

    @Override
    public void review() {
        if (this.sprint.getSprintType() == SprintType.DEVELOPMENT){
            this.sprint.setState(new ReviewState(this.sprint));
        }
        else Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a review state");
    }

    @Override
    public void done() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has not been deployed / reviewed yet");
    }
}
