package nl.avans.sprint;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DoneState implements SprintState{
    private final Sprint sprint;

    public DoneState(Sprint sprint){
        this.sprint = sprint;
    }

    private void logger(){
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint is already completed");
    }

    @Override
    public void create() {
        logger();
    }

    @Override
    public void execute() {
        logger();
    }

    @Override
    public void deploy() {
        logger();
    }

    @Override
    public void review() {
        logger();
    }

    @Override
    public void done() {
        logger();
    }
}
