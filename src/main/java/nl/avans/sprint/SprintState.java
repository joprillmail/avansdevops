package nl.avans.sprint;

public interface SprintState {
    void create();
    void execute();
    void deploy();
    void review();
    void done();
}
