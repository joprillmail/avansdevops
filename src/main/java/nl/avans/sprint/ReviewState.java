package nl.avans.sprint;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ReviewState implements SprintState {
    private final Sprint sprint;

    public ReviewState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void create() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has already been created");
    }

    @Override
    public void execute() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint has already been executed");
    }

    @Override
    public void deploy() {
        Logger.getAnonymousLogger().log(Level.INFO, "This type of sprint does not support a deploy state");
    }

    @Override
    public void review() {
        Logger.getAnonymousLogger().log(Level.INFO, "Sprint is already in review state");
    }

    @Override
    public void done() {
        this.sprint.setState(new DoneState(this.sprint));
    }
}
