package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MailNotification {
    public void sendMailNotification(BacklogItem subject){
        Logger.getAnonymousLogger().log(Level.INFO, "Send mail about " + subject.getTaskName());
    }
}
