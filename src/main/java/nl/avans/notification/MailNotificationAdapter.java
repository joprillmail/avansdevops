package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

public class MailNotificationAdapter implements NotificationSender{
    private MailNotification mailNotification;

    public MailNotificationAdapter() {
        this.mailNotification = new MailNotification();
    }

    @Override
    public void sendNotification(BacklogItem subject) {
        mailNotification.sendMailNotification(subject);
    }
}
