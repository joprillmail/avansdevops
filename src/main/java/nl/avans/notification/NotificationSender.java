package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

public interface NotificationSender {
    void sendNotification(BacklogItem subject);
}
