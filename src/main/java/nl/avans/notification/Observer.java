package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

public abstract class Observer {
    protected BacklogItem subject;
    public abstract void update();
}
