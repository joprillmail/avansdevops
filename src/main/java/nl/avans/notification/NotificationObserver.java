package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

import java.util.logging.Level;
import java.util.logging.Logger;

public class NotificationObserver extends Observer{
    private final MailNotificationAdapter mailNotificationAdapter = new MailNotificationAdapter();
    private final ConsoleNotification consoleNotification = new ConsoleNotification();

    public NotificationObserver(BacklogItem subject){
        this.subject = subject;
        this.subject.registerObserver(this);
    }

    public MailNotificationAdapter getMailNotificationAdapter() {
        return mailNotificationAdapter;
    }

    public ConsoleNotification getConsoleNotification() {
        return consoleNotification;
    }

    @Override
    public void update() {
        mailNotificationAdapter.sendNotification(this.subject);
        consoleNotification.sendNotification(this.subject);

        Logger.getAnonymousLogger().log(Level.INFO, "Notification sent!");
    }
}
