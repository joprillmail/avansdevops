package nl.avans.notification;

import nl.avans.backlogitem.BacklogItem;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleNotification implements NotificationSender{
    @Override
    public void sendNotification(BacklogItem subject) {
        Logger.getAnonymousLogger().log(Level.INFO, "Notify about " + subject.getTaskName());
    }
}
