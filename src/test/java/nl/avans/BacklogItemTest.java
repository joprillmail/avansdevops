package nl.avans;

import nl.avans.backlogitem.BacklogItem;
import nl.avans.user.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.beans.BeanProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BacklogItemTest {
    private List<User> users = new ArrayList<>();

    @Before
    public void init(){
        var studentDev = new Student("Kees Karel", "keeskarel@avans.nl", ProjectRole.DEVELOPER, "password123");
        var studentTest = new Student("Hans Kazam", "hanshaarlem@avans.nl", ProjectRole.TESTER, "password123");
        var studentLeadDev = new Student("Pieter Peter", "pieterpeter@avans.nl", ProjectRole.LEAD_DEVELOPER, "password123");
        var teacherScrumMaster = new Teacher("Jeffry Jeston", "jeffryjeston@avans.nl", ProjectRole.SCRUM_MASTER, "password123");
        var otherProdOwner = new Other("Mustafa Mazuku", "mustafa@mazuku.mk", ProjectRole.PRODUCT_OWNER, "password123");

        users.add(studentDev);
        users.add(studentTest);
        users.add(studentLeadDev);
        users.add(teacherScrumMaster);
        users.add(otherProdOwner);
    }

    @Test
    public void testIfBacklogItemCanBeCreated(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Do something", "Lorem Ipsum", 5);

        // Assert
        Assert.assertNotNull(backlogItem);
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTodoToTodo(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().todo();

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTodoToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));

        // Assert
        Assert.assertEquals("DoingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTodoToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().readyForTesting();

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTodoToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().testing(users.get(2));

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTodoToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().tested(users.get(3));

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTodoToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().done();

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromDoingToTodo(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().todo();

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoingToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().doing(users.get(0));

        // Assert
        Assert.assertEquals("DoingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromDoingToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoingToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().testing(users.get(1));

        // Assert
        Assert.assertEquals("DoingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoingToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().tested(users.get(2));

        // Assert
        Assert.assertEquals("DoingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoingToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().done();

        // Assert
        Assert.assertEquals("DoingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromReadyForToTodo(){
        // ArrangeTesting
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().todo();

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromReadyForTestingToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().doing(users.get(0));

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromReadyForTestingToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().readyForTesting();

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromReadyForTestingToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromReadyForTestingToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().tested(users.get(2));

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromReadyForTestingToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().done();

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestingToTodo(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().todo();

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestingToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().doing(users.get(0));

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestingToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().readyForTesting();

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestingToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().testing(users.get(1));

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTestingToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));

        // Assert
        Assert.assertEquals("TestedState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestingToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().done();

        // Assert
        Assert.assertEquals("TestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTestedToTodo(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().todo();

        // Assert
        Assert.assertEquals("TodoState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestedToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().doing(users.get(0));

        // Assert
        Assert.assertEquals("TestedState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTestedToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().readyForTesting();

        // Assert
        Assert.assertEquals("ReadyForTestingState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestedToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().testing(users.get(1));

        // Assert
        Assert.assertEquals("TestedState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromTestedToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().tested(users.get(2));

        // Assert
        Assert.assertEquals("TestedState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanChangeStateFromTestedToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToTodo(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().todo();

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToDoing(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().doing(users.get(0));

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToReadyForTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().readyForTesting();

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToTesting(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().testing(users.get(1));

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToTested(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().tested(users.get(2));

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfBacklogItemCanNotChangeStateFromDoneToDone(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));
        backlogItem.getDoingState().readyForTesting();
        backlogItem.getReadyForTestingState().testing(users.get(1));
        backlogItem.getTestingState().tested(users.get(2));
        backlogItem.getTestedState().done();
        backlogItem.getDoneState().done();

        // Assert
        Assert.assertEquals("DoneState", backlogItem.getState());
    }

    @Test
    public void testIfAUserCanBeAssignedToABacklogItem(){
        // Arrange
        BacklogItem backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);

        // Act
        backlogItem.getTodoState().doing(users.get(0));

        // Assert
        Assert.assertEquals(users.get(0), backlogItem.getUser());
    }
}
