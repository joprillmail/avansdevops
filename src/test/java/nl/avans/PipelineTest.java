package nl.avans;

import nl.avans.pipeline.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PipelineTest {

    @Before
    public void init(){
        // TODO

    }

    @Test
    public void testIfAPipelineCanBeCreated(){
        // Arrange
        var pipeline = new Pipeline("test pipeline");

        // Assert
        Assert.assertNotNull(pipeline);
        Assert.assertEquals("test pipeline", pipeline.getPipelineName());
    }

    @Test
    public void testIfAPipelineCanContainAnImageAndJob(){
        // Arrange
        var pipeline = new Pipeline("test pipeline");

        // Act
        var image = new Image("maven:4.8.3-openjdk");
        var job = new Job("TestJob");
        pipeline.addComponent(image);
        pipeline.addComponent(job);

        // Assert
        Assert.assertEquals(2, pipeline.getSize());
        Assert.assertEquals(pipeline.getComponent(0), image);
        Assert.assertEquals(pipeline.getComponent(1), job);
    }

    @Test
    public void testIfAJobCanContainACommand(){
        // Arrange
        var pipeline = new Pipeline("Test pipeline");
        var job = new Job("TestJob");

        pipeline.addComponent(job);

        // Act
        var cmd = new Command("mvn test");
        job.addComponent(cmd);

        // Assert
        Assert.assertEquals(pipeline.getComponent(0), job);
        Assert.assertEquals(job.getComponent(0), cmd);
    }

    @Test
    public void testIfAJobCanContainAnImage(){
        // Arrange
        var pipeline = new Pipeline("Test pipeline");
        var job = new Job("TestJob");
        pipeline.addComponent(job);

        // Act
        var image = new Image("dotnet:6.0.0");
        job.addComponent(image);

        // Assert
        Assert.assertEquals(pipeline.getComponent(0), job);
        Assert.assertEquals(job.getComponent(0), image);
    }

    @Test
    public void testIfAPipelineCanBeExecuted(){
        // Arrange
        var pipeline = new Pipeline("TestPipeline");
        var image = new Image("node:16");
        var job = new Job("test");
        var cmdNpmCi = new Command("npm ci");
        var cmdNpmTest = new Command("npm test");

        pipeline.addComponent(image);
        pipeline.addComponent(job);
        job.addComponent(cmdNpmCi);
        job.addComponent(cmdNpmTest);

        var visitor = Mockito.spy(new LoggingVisitor());

        // Act
        pipeline.acceptVisitor(visitor);

        // Assert
        Mockito.verify(visitor).visitPipeline(pipeline);
        Mockito.verify(visitor).visitImage(image);
        Mockito.verify(visitor).visitJob(job);
        Mockito.verify(visitor).visitCommand(cmdNpmCi);
        Mockito.verify(visitor).visitCommand(cmdNpmTest);
    }
}
