package nl.avans;

import nl.avans.user.*;
import org.junit.Assert;
import org.junit.Test;

public class UserTest {

    @Test
    public void testIfAStudentCanBeCreatedUsingFactory(){
        // Arrange
        var userFactory = new UserFactory();

        // Act
        var student = userFactory.registerUser(UserType.STUDENT, "keesie12", "keesie12@avans.nl", ProjectRole.DEVELOPER, "password123");

        // Assert
        Assert.assertNotNull(student);
        Assert.assertEquals(Student.class, student.getClass());
    }

    @Test
    public void testIfTeacherCanBeCreatedUsingFactory(){
        // Arrange
        var userFactory = new UserFactory();

        // Act
        var teacher = userFactory.registerUser(UserType.TEACHER, "Docent_Pieter", "docentpieter@avans.nl", ProjectRole.SCRUM_MASTER, "password123");

        // Assert
        Assert.assertNotNull(teacher);
        Assert.assertEquals(Teacher.class, teacher.getClass());
    }

    @Test
    public void testIfOtherCanBeCreatedUsingFactory(){
        // Arrange
        var userFactory = new UserFactory();

        // Act
        var other = userFactory.registerUser(UserType.OTHER, "Peter Pieterson", "pieter_pieterson@bxt.io", ProjectRole.PRODUCT_OWNER, "password123");

        // Assert
        Assert.assertNotNull(other);
        Assert.assertEquals(Other.class, other.getClass());
    }
}
