package nl.avans;

import nl.avans.backlogitem.BacklogItem;
import nl.avans.sprint.Sprint;
import nl.avans.sprint.SprintType;
import nl.avans.user.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SprintTest {
    private BacklogItem backlogItem;
    private List<User> users = new ArrayList<>();

    @Before
    public void init(){
        backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);
        var studentDev = new Student("Kees Karel", "keeskarel@avans.nl", ProjectRole.DEVELOPER, "password123");
        var studentTest = new Student("Hans Kazam", "hanshaarlem@avans.nl", ProjectRole.TESTER, "password123");
        var studentLeadDev = new Student("Pieter Peter", "pieterpeter@avans.nl", ProjectRole.LEAD_DEVELOPER, "password123");
        var teacherScrumMaster = new Teacher("Jeffry Jeston", "jeffryjeston@avans.nl", ProjectRole.SCRUM_MASTER, "password123");
        var otherProdOwner = new Other("Mustafa Mazuku", "mustafa@mazuku.mk", ProjectRole.PRODUCT_OWNER, "password123");

        users.add(studentDev);
        users.add(studentTest);
        users.add(studentLeadDev);
        users.add(teacherScrumMaster);
        users.add(otherProdOwner);
    }

    @Test
    public void testIfSprintsCanBeCreated(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Assert
        Assert.assertNotNull(devSprint);
        Assert.assertNotNull(deploySprint);
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintItsStateCanNotChangeFromCreateToCreate(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().create();
        deploySprint.getCreateState().create();

        // Assert
        Assert.assertEquals("CreateState", devSprint.getState());
        Assert.assertEquals("CreateState", deploySprint.getState());
    }

    @Test
    public void testIfADevelomentOrDepoymentSprintItsStateCanChangeStateFromCreateToExecute(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        deploySprint.getCreateState().execute();

        // Assert
        Assert.assertEquals("ExecuteState", devSprint.getState());
        Assert.assertEquals("ExecuteState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromCreateToDeploy(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().deploy();
        deploySprint.getCreateState().deploy();

        // Assert
        Assert.assertEquals("CreateState", devSprint.getState());
        Assert.assertEquals("CreateState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromCreateToReview(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().review();
        deploySprint.getCreateState().review();

        // Assert
        Assert.assertEquals("CreateState", devSprint.getState());
        Assert.assertEquals("CreateState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromCreateToDone(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().done();
        deploySprint.getCreateState().done();

        // Assert
        Assert.assertEquals("CreateState", devSprint.getState());
        Assert.assertEquals("CreateState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromExecuteToCreate(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().create();
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().create();

        // Assert
        Assert.assertEquals("ExecuteState", devSprint.getState());
        Assert.assertEquals("ExecuteState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromExecuteToExecute(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().execute();
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().execute();

        // Assert
        Assert.assertEquals("ExecuteState", devSprint.getState());
        Assert.assertEquals("ExecuteState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanChangeStateFromExecuteToDeploy(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();

        // Assert
        Assert.assertEquals("DeployState", deploySprint.getState());
    }

    @Test
    public void testIfDevelopmentSprintCanNotChangeStateFromExecuteToDeploy(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().deploy();

        // Assert
        Assert.assertEquals("ExecuteState", devSprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanNotChangeStateFromExecuteToReview(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().review();

        // Assert
        Assert.assertEquals("ExecuteState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanChangeStateFromExecuteToReview(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();

        // Assert
        Assert.assertEquals("ReviewState", devSprint.getState());
    }

    @Test
    public void testIfADevelopmentOrDeploymentSprintCanNotChangeStateFromExecuteToDone(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().done();
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().done();

        // Assert
        Assert.assertEquals("ExecuteState", devSprint.getState());
        Assert.assertEquals("ExecuteState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanNotChangeStateFromDeployToCreate(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().create();

        // Assert
        Assert.assertEquals("DeployState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanNotChangeStateFromDeployToExecute(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().execute();

        // Assert
        Assert.assertEquals("DeployState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanNotChangeStateFromDeployToDeploy(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().deploy();

        // Assert
        Assert.assertEquals("DeployState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanNotChangeStateFromDeployToReview(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().review();

        // Assert
        Assert.assertEquals("DeployState", deploySprint.getState());
    }

    @Test
    public void testIfADeploymentSprintCanChangeStateFromDeployToDone(){
        // Arrange
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();

        // Assert
        Assert.assertEquals("DoneState", deploySprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanNotChangeStateFromReviewToCreate(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().create();

        // Assert
        Assert.assertEquals("ReviewState", devSprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanNotChangeStateFromReviewToExecute(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().execute();

        // Assert
        Assert.assertEquals("ReviewState", devSprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanNotChangeStateFromReviewToDeploy(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().deploy();

        // Assert
        Assert.assertEquals("ReviewState", devSprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanNotChangeStateFromReviewToReview(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().review();

        // Assert
        Assert.assertEquals("ReviewState", devSprint.getState());
    }

    @Test
    public void testIfADevelopmentSprintCanChangeStateFromReviewToDone(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfDevelopmentOrDeploymentSprintCanNotChangeStateFromDoneToCreate(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();
        devSprint.getDoneState().create();

        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();
        deploySprint.getDoneState().create();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfDevelopmentOrDeploymentSprintCanNotChangeStateFromDoneToExecute(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();
        devSprint.getDoneState().execute();

        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();
        deploySprint.getDoneState().execute();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfDevelopmentOrDeploymentSprintCanNotChangeStateFromDoneToDeploy(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();
        devSprint.getDoneState().deploy();

        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();
        deploySprint.getDoneState().deploy();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfDevelopmentOrDeploymentSprintCanNotChangeStateFromDoneToReview(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();
        devSprint.getDoneState().review();

        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();
        deploySprint.getDoneState().review();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfDevelopmentOrDeploymentSprintCanNotChangeStateFromDoneToDone(){
        // Arrange
        var devSprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var deploySprint = new Sprint(2, "1st deploy sprint", "Lorem Ipsum", SprintType.DEPLOYMENT);

        // Act
        devSprint.getCreateState().execute();
        devSprint.getExecuteState().review();
        devSprint.getReviewState().done();
        devSprint.getDoneState().done();

        deploySprint.getCreateState().execute();
        deploySprint.getExecuteState().deploy();
        deploySprint.getDeployState().done();
        deploySprint.getDoneState().done();

        // Assert
        Assert.assertEquals("DoneState", devSprint.getState());
        Assert.assertEquals("DoneState", devSprint.getState());
    }

    @Test
    public void testIfABacklogItemCanBeAddedToASprint(){
        // Arrange
        var sprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var backlogItem = new BacklogItem(1, "Do something", "Lorem Ipsum", 5);

        // Act
        sprint.addBacklogItem(backlogItem);

        // Assert
        Assert.assertEquals(1, sprint.getBacklogItems().size());
        Assert.assertEquals(backlogItem, sprint.getBacklogItems().get(0));
    }

    @Test
    public void testIfABacklogItemCanBeRemovedFromASprint(){
        // Arrange
        var sprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        var backlogItem = new BacklogItem(1, "Do something", "Lorem Ipsum", 5);
        sprint.addBacklogItem(backlogItem);

        // Act
        sprint.removeBacklogItem(backlogItem);

        // Assert
        Assert.assertEquals(0, sprint.getBacklogItems().size());
    }

    @Test
    public void testIfAnUserCanBeAddedToASprint(){
        // Arrange
        var sprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);

        // Act
        sprint.addUser(users.get(0));

        // Assert
        Assert.assertEquals(1, sprint.getUsers().size());
    }

    @Test
    public void testIfAnUserCanBeRemovedFromASprint() {
        // Arrange
        var sprint = new Sprint(1, "1st dev sprint", "Lorem Ipsum", SprintType.DEVELOPMENT);
        sprint.addUser(users.get(0));

        // Act
        sprint.removeUser(users.get(0));

        // Assert
        Assert.assertEquals(0, sprint.getUsers().size());
    }
}
