package nl.avans;

import nl.avans.backlogitem.BacklogItem;
import nl.avans.notification.NotificationObserver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

public class NotificationTest {
    private BacklogItem backlogItem;

    @Before
    public void init(){
        backlogItem = new BacklogItem(1, "Make code", "Code has to be made", 2);
    }

    @Test
    public void testIfANotificationObserverCanBeRegistered(){
        // Arrange
        var notificationObserver = new NotificationObserver(backlogItem);

        // Assert
        Assert.assertEquals(1, backlogItem.getObservers().size());
        Assert.assertEquals(notificationObserver, backlogItem.getObservers().get(0));
    }

    @Test
    public void testIfANotificationObserverCanBeRemove(){
        // Arrange
        var notificationObserver = new NotificationObserver(backlogItem);

        // Act
        backlogItem.removeObserver(notificationObserver);

        // Assert
        Assert.assertEquals(0, backlogItem.getObservers().size());
    }

    @Test
    public void testIfANotificationObserverHasBeenNotified(){
        // Arrange
        new NotificationObserver(backlogItem);
        var observers = Mockito.spy(backlogItem.getObservers());

        // Act
        backlogItem.notifyObservers();

        // Assert
        observers.forEach(observer -> {
            Mockito.verify(observer).update();
        });
    }

    @Test
    public void testIfANotificationCanBeSendViaMail(){
        // Arrange
        var notificationObserver = new NotificationObserver(backlogItem);
        var mailAdapterSpy = Mockito.spy(notificationObserver.getMailNotificationAdapter());

        // Act
        mailAdapterSpy.sendNotification(backlogItem);

        // Assert
        Mockito.verify(mailAdapterSpy).sendNotification(backlogItem);
    }

    @Test
    public void testIfANotificationCanBeSendViaConsole(){
        // Arrange
        var notificationObserver = new NotificationObserver(backlogItem);
        var consoleNotification = Mockito.spy(notificationObserver.getConsoleNotification());

        // Act
        consoleNotification.sendNotification(backlogItem);

        // Assert
        Mockito.verify(consoleNotification).sendNotification(backlogItem);
    }
}
